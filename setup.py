#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup

setup(name='mubody',
      version='0.0.1',
      description="Multi-body dynamics library as a PhD tool",
      url='',
      author='Juan Bermejo',
      author_email='juan.bermejo@upm.es',
      license='MIT',
      packages=['mubody'],
      zip_safe=False)
