import os


def save_eph(mission, file_name):
    mission.coordinate_frame_change('SunJ2000Eq')
    tra_df = mission.sat.orbit.tra_df
    date = mission.DS.date
    txt = tra_df.to_string(header=False)
    times = tra_df.index.values

    file_path = "Results"

    os.makedirs(file_path, exist_ok=True)

    f = open(file_path + "/" + file_name + '.e', 'w+')

    f.write(('stk.v.10.0'
             + '\n# WrittenBy    GMAT R2018a'
             + '\nBEGIN Ephemeris'
             + '\nNumberOfEphemerisPoints ' + str(len(times))
             + '\nScenarioEpoch ' + date.strftime('%d %b %Y %H:%M:%S.%f')
             + '\nCentralBody             Sun'
             + '\nCoordinateSystem        J2000'
             + '\nDistanceUnit            Meters'
             + '\n'
             + '\nEphemerisTimePosVel'
             + '\n'
             + '\n'
             + txt
             + '\n'
             + '\nEND Ephemeris'))

    f.close()
