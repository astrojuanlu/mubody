======
mubody
======

Description
===========

mubody is an open source Python library focused in multi-body dynamics. It's aim is to be used as a tool for the analysis of space missions in the Lagrangian points. It has been developed by students from UPM.


Installation
============

Please check the `Installation Guide`_ in the project `wiki`_.

.. _`Installation Guide`:  https://gitlab.com/juan.b/mubody/-/wikis/Installation%20Guide


Documentation
=============

Basic documentation can be found `here`_ and some examples can also be found in the project `wiki`_.

.. _`wiki`: https://gitlab.com/juan.b/mubody/-/wikis

.. _`here`: https://mubody.readthedocs.io/


Contact
============

juan.bermejo@upm.es

